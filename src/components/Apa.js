require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';

class Apa extends React.Component {
  constructor(){
    super();
    this.getGame = this.getGame.bind(this);
  }

  componentDidMount(){
    this.getGame();
  }

  getGame(){
    var component = this;
    console.log(this.props.params.id)
    var id = this.props.params.id;
    $.ajax({
      type: "GET",
      url: "http://localhost:3001/games/" + id,
    }).then(function(data) {
      component.setState({players: data})
    });
  }

  render() {
    var playerNames = [];

    for(var i = 0; i < this.state.players.length; i++){
      playerNames.push(<Player name={} />)
    }
    return (
      <div className="game">

      </div>
    );
  }
}

export default Apa;
