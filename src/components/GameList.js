require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import Game from './Game'

class GameList extends React.Component {
  constructor(){
    super();
    this.handleclick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {gameList: []}
  }

  handleChange(event){
    this.setState({gameName: event.target.value})
    console.log(event.target.value)
  }
  componentDidMount(){
    this.getGameList();
  }

  updateGameList(data){
    this.setState({gameName: data})
  }
  getGameList(){
    var component = this;
    $.ajax({
      type: "GET",
      url: "http://localhost:3001/games",
    }).then(function(data) {
      component.setState({gameList: data})
    });
  }

  handleClick(){
    $.ajax({
      type: "POST",
      url: "http://localhost:3001/games",
      crossDomain: true,
      data: this.state.gameName
    }).then(function (data) {
      console.log(data);
    });
  }
  render() {
    var gameList = this.state.gameList;
    var gameNames = [];
    for(var i = 0; i < gameList.length; i++) {
      var gameName = gameList[i];
      gameNames.push(<Game name={gameName} key={i}/>)
    }
    return (
      <div className="gameList">
        <ul>
          {gameNames}
        </ul>
      </div>
    );
  }
}

GameList.defaultProps = {
};

export default GameList;
