require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import GameIntro from './GameIntro'
import Apa from './Apa'
import {Router, Route, Link, browserHistory} from 'react-router';

let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={GameIntro} />
        <Route path="/:id" component={Apa} />
      </Router>
    );
  }
}

AppComponent.defaultProps = {};

export default AppComponent;
