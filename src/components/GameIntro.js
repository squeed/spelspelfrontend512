require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import GameList from './GameList'
import {Router, Route, Link} from 'react-router';

let yeomanImage = require('../images/yeoman.png');

class GameIntro extends React.Component {
  constructor() {
    super();
    this.handleclick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {gameName: ""}
  }

  handleChange(event) {
    this.setState({gameName: event.target.value})
    console.log(event.target.value)
  }

  handleClick() {
    $.ajax({
      contentType: 'application/json',
      dataType: 'json',
      type: "POST",
      url: "http://localhost:3001/games",
      crossDomain: true,
      data: JSON.stringify({gameName: this.state.gameName})
    }).then(function (data) {
      console.log(data);
    });
  }

  render() {
    return (
      <div className="index">
        <div>Welcome toa da gaaaaaaaaame!</div>
        <input type="text" value={this.state.gameName} onChange={this.handleChange}/>
        <button onClick={this.handleclick}>CREATE AS FAK</button>

        <GameList />
      </div>
    );
  }
}

GameIntro.defaultProps = {};

export default GameIntro;
