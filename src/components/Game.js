require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';

class Game extends React.Component {
  constructor(){
    super();
    this.state = {gameList: []};
    this.joinGame = this.joinGame.bind(this);
  }

  joinGame(){
    window.location.href = window.location.href + this.props.name;
  }

  render() {
    var name = this.props.name;
    return (
      <div className="game">
        <ul>
          <li>
            {name} <button onClick={this.joinGame}>Join</button>
          </li>
        </ul>
      </div>
    );
  }
}

Game.defaultProps = { name: ""
};

export default Game;
